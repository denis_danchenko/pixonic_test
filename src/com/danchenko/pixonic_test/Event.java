package com.danchenko.pixonic_test;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * Created by danch on 10.04.2016.
 */
public class Event {
    private Integer id;
    private Date dateTime;
    private Callable<Integer> callable;
    private boolean processed = false;

    public Event(Integer id, Date dateTime, Callable<Integer> callable) {
        this.id = id;
        this.dateTime = dateTime;
        this.callable = callable;
    }

    public Integer getId() {
        return id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public Integer process() throws Exception {
        if (!processed) {
            processed = true;
            System.out.println("Executing callable of event " + id + " on " + Main.SDF.format(dateTime));
            return callable.call();
        } else {
            return -1;
        }
    }

    public boolean isProcessed() {
        return processed;
    }

}
