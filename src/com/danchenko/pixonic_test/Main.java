package com.danchenko.pixonic_test;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

public class Main implements Runnable {

    public static final int MAX_THREADS = 1;
    public static final int OVERLOAD_FACTOR = 2;
    public static final int MAX_OFFSET_MSEC = 1000;
    public static final Random RND = new Random();

    public static SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss.ms");

    public static volatile ConcurrentSkipListSet<Event> events = new ConcurrentSkipListSet<>(new Comparator<Event>() {
        @Override
        public int compare(Event o1, Event o2) {
            int result = o1.getDateTime().compareTo(o2.getDateTime());
            return result == 0 ? o1.getId().compareTo(o2.getId()) : result;
        }
    });
    public static final int eventMutex = 0;

    private List<Thread> generators = new ArrayList<>();
    private List<Thread> workers = new ArrayList<>();

    public static void main(String[] args) {
        new Main().run();
    }

    @Override
    public void run() {
        for (int i=0; i < MAX_THREADS * OVERLOAD_FACTOR; i++) {
            Thread thread = new Thread(new EventGenerator());
            generators.add(thread);
            thread.start();
        }

        for (int i=0; i < MAX_THREADS; i++) {
            Thread thread = new Thread(new EventWorker());
            workers.add(thread);
            thread.start();
        }
    }
}
