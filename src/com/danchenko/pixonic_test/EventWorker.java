package com.danchenko.pixonic_test;

import java.util.Date;

/**
 * Created by danch on 10.04.2016.
 */
public class EventWorker implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                Event event = Main.events.first();
                if (!event.getDateTime().after(new Date())) {
                    if (Main.events.remove(event)) {
                        event.process();
                    }
                }
                Thread.sleep(1000);
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }
}
