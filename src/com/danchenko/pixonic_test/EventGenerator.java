package com.danchenko.pixonic_test;

import java.util.Calendar;

/**
 * Created by danch on 10.04.2016.
 */
public class EventGenerator implements Runnable {
    public static volatile int eventId = 1;

    @Override
    public void run() {
        while (true) {
            int offsetMSec = Main.RND.nextInt(Main.MAX_OFFSET_MSEC);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MILLISECOND, offsetMSec);
            Event event = new Event(eventId++, calendar.getTime(), new LogCallable());
            if (Main.events.add(event)) {
                System.out.println("Event " + event.getId() + " generated at " + Main.SDF.format(calendar.getTime()));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
