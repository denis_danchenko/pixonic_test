package com.danchenko.pixonic_test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

/**
 * Created by danch on 10.04.2016.
 */
public class LogCallable implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        System.out.println("Callable completed at " + Main.SDF.format(new Date()));
        return 0;
    }
}
